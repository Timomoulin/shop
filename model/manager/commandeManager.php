<?php
//require_once("model/class/article.class.php");
class CommandeManager{
private $lePDO;

public function __construct($unPDO)
{
    $this->lePDO=$unPDO;
}


function createCommande(){
    try {
        //idCommande 	dateCommande 	dateLivraison 	etat 	idClient 
        $connex=$this->lePDO;
        $connex->beginTransaction();
        $sql =$connex->prepare("INSERT INTO commande values(null,:dateCommande,null,'En cours',:idClient)");
        $today = date("Y-m-d H:i:s");   
        $sql->bindParam(":dateCommande",$today);
        $sql->bindValue(":idClient",$_SESSION['id']);
        $sql->execute();

        //idArticle 	idCommande 	quantiteArticle 
        $idCommande=$connex->lastInsertId();
        foreach($_SESSION['panier'] as $uneLignePanier)
        {
        $sql =$connex->prepare("INSERT INTO article_commande values(:idArticle,:idCommande,:quantite)");
        
        $sql->bindParam(":idCommande",$idCommande);
        $sql->bindValue(":idArticle",$uneLignePanier[0]);
        $sql->bindValue(":quantite",$uneLignePanier[1]);
        $sql->execute();
        }
        $connex->commit();

    } catch (PDOException $error) {
        $connex->rollBack();
        echo $error->getMessage();
    }
}
}
?>