<?php
require_once("model/class/categorie.class.php");
class CategorieManager{
private $lePDO;

public function __construct($unPDO)
{
    $this->lePDO=$unPDO;
}

/**
 * Fonction qui permet de recuperer toutes les categories
 *
 * @return array Les categories dans un array a 2 dimensions
 */
function fetchAllCategorie(){

    try {
        //Pour la co on utilise l'attribut lePDO
        $connex=$this->lePDO;
        $sql =$connex->prepare("SELECT * FROM categorie ORDER BY nom");
        $sql->execute();
        $sql->setFetchMode(PDO::FETCH_CLASS,"Categorie");
        $resultat = ($sql->fetchAll());
        return $resultat;

    } catch (PDOException $error) {
        echo $error->getMessage();
    }
}

 /**
     * Permet de recuperer une categorie par son id
     *
     * @param [type] $id l'id de la categorie
     * @return array la categorie sous la forme d'un array
     */
    function fetchCategorieById($id)
    {
        try {
            $connex=$this->lePDO;
            $sql =$connex->prepare("SELECT * FROM categorie WHERE idCategorie=:idCategorie");
            $sql->bindParam(":idCategorie",$id);
            $sql->execute();
            $sql->setFetchMode(PDO::FETCH_CLASS,"Categorie");
            $resultat = ($sql->fetch());
            return $resultat;

        } catch (PDOException $error) {
            echo $error->getMessage();
        }
    }

}
?>