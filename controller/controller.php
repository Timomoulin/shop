<?php
require_once("model/manager/categorieManager.php");
require_once("model/manager/articleManager.php");
require_once("model/manager/clientManager.php");
if(!isset($_GET['action']))
{
    $action="home";
}
else{
    $action=$_GET['action'];
}

switch ($action){
    case "home":
        require('view/home.php');
    break;

    case "contact" :
        require ('view/contact.php');
    break;

    case "categorie":
        $idCateg=filter_var($_GET['id'],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $objetCatManager=new CategorieManager($lePDO);
        $laCategorie=$objetCatManager->fetchCategorieById($idCateg);

        $objetArticleManager= new ArticleManager($lePDO);
        $lesArticles=$objetArticleManager->fetchAllArticleByIdCateg($idCateg);
        require ('view/categorie/categorie.php');
    break;

    case "article":
        $idArticle=filter_var($_GET['id'],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $objetArticleManager=new ArticleManager($lePDO);
        $article=$objetArticleManager->fetchArticleByIdArticle($idArticle);
        
        require("view/article/viewArticle.php");
    break;

    case "formLogin":
        
        require("view/client/login.php");
    break;

    case "traitementLogin":
        var_dump($_POST);
        $email=filter_var($_POST['email'],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $mdp=filter_var($_POST['mdp'],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $mdp=hash("sha256",$mdp);
      
        $objetClientManager=new ClientManager($lePDO);
        $client=$objetClientManager->fetchClientByEmailAndMdp($email,$mdp);
        if(empty($client)){
            //msg $msgErreur
            $_SESSION['erreur']= [];
            array_push($_SESSION['erreur'],"Erreur connexion");
            header("location:./?path=main&action=formLogin");
        }
        else{
            $_SESSION["email"]=$client->getEmail();
            $_SESSION["id"]=$client->getIdClient();
            $_SESSION["role"]="client";
            header("location:./");
      
        }
        
        // @todo 
    break;

    case "logout":
        session_unset();
        session_destroy();
        header("location:./?path=main&action=formLogin");
        break;

    case "formInscription":
        require("view/client/inscription.php");
    break;
    
    case "traitementInscription" :
        //@todo 
    break;

    default :
    require('view/404.php');
}

?>